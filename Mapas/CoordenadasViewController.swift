//
//  CoordenadasViewController.swift
//  Mapas
//
//  Created by Daniel Gomez on 13/02/2019.
//  Copyright © 2019 Daniel Gomez. All rights reserved.
//

import UIKit
import CoreLocation

class CoordenadasViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var labelLatitud: UILabel!
    @IBOutlet weak var labelLongitud: UILabel!
    var manager = CLLocationManager()
    var latitud : CLLocationDegrees!
    var longitud : CLLocationDegrees!
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.manager.delegate = self
        self.manager.requestWhenInUseAuthorization()
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        self.manager.startUpdatingLocation()
    }
    
    // MARK: -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.latitud = location.coordinate.latitude
            self.longitud = location.coordinate.longitude
        }
    }
    
    // MARK: -

    @IBAction func verCoordenadas(_ sender: UIButton) {
        self.labelLatitud.text = "Latitud: \(self.latitud!)"
        self.labelLongitud.text = "Longitud: \(self.longitud!)"
    }
    
    @IBAction func enviarCoordenadas(_ sender: UIButton) {
    }
}

//
//  ViewController.swift
//  Mapas
//
//  Created by Daniel Gomez on 13/02/2019.
//  Copyright © 2019 Daniel Gomez. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet var searchBarBuscador: UISearchBar!
    @IBOutlet weak var mapViewMapa: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBarBuscador.delegate = self
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let geocoder = CLGeocoder()
        
        searchBarBuscador.resignFirstResponder()
        geocoder.geocodeAddressString(searchBarBuscador.text!) { (places: [CLPlacemark]?, error: Error?) in
            if error == nil {
                let place = places?.first
                let anotacion = MKPointAnnotation()
                anotacion.coordinate = (place?.location?.coordinate)!
                anotacion.title = self.searchBarBuscador.text
                
                let span = MKCoordinateSpan(latitudeDelta: 0.10,longitudeDelta: 0.10)
                let region = MKCoordinateRegion(center: anotacion.coordinate, span: span)
                self.mapViewMapa.setRegion(region, animated: true)
                
                self.mapViewMapa.addAnnotation(anotacion)
                self.mapViewMapa.selectAnnotation(anotacion, animated: true)
            }else{
                print("Hubo un error")
            }
        }
    
    }


}

